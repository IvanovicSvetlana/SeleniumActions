package execomsajtvezba;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import execomvezba.ExecomHomePage;
import execomvezba.ReserveSpotAtExecom;

public class ExecomTest {
	private WebDriver driver;
	private ExecomHomePage execom;
	private ReserveSpotAtExecom reserve;
	@BeforeMethod
	public void setupSelenium() {
		String baseUrl = "https://www.execom.eu";
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.navigate().to(baseUrl);

execom =new ExecomHomePage(driver);
reserve= new ReserveSpotAtExecom(driver);
	}
  @Test(enabled=false)
  public void getHomePage() {
Assert.assertTrue(execom.isTitleTextPresent());
Assert.assertTrue(execom.getCareers().isDisplayed());
execom.getCareers().click();
Assert.assertTrue(execom.getWeliketochallengethestatusquoText().isDisplayed());
Assert.assertTrue(execom.isSoftwareTesterTextPresent());
Assert.assertTrue(execom.getSoftwareTesterTextToBeClicked().contains("Tester"));
  }
  @Test
  public void ReserveaCupOfCoffe(){
	  driver.navigate().to("https://www.execom.eu/blog/post/software-tester-novi-sad-execom");
	Assert.assertTrue(reserve.getReserveACoffe().isDisplayed());
//Assert.assertTrue(reserve.getReserveACoffe().getText().contains("No CVs please"));
  }
 /* @Test
  public void getHeaderBtn() {
execom.getHeaderBtn().click();
execom.getCareers().click();
Assert.assertTrue(execom.isSoftwareTesterTextPresent());
  }*/
  @AfterMethod
  public void closeSelenium() {
  	// Shutdown the browser
  	driver.quit();
  }
  
}
