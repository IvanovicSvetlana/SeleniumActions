package execomvezba;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ReserveSpotAtExecom {
private WebDriver driver;

public ReserveSpotAtExecom(WebDriver driver) {
	super();
	this.driver = driver;
}
public WebElement getReserveACoffe() {
	return Utils.waitToBeClickable(driver, By.xpath("//a[contains(text(),'Reserve a cup')]"), 10);
}

public WebElement getNoCvRequired() {
	return Utils.waitToBeClickable(driver, By.xpath("//a[contains(text(),'Reserve a cup')]/em"), 10);
}
}
