package execomvezba;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Utils {
	
	public static void takeSnapShot(WebDriver webdriver, String fileWithPath) throws IOException{
		// Convert web driver object to TakeScreenshot
				TakesScreenshot scrShot = ((TakesScreenshot) webdriver);
				// Call getScreenshotAs method to create image file
				File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
				// Move image file to new destination
				File DestFile = new File(fileWithPath);
				// Copy file at destination
				FileUtils.copyFile(SrcFile, DestFile);
	}
	
	
	public static boolean isPresent(WebDriver webdriver, By selector) {
		
		try {
			webdriver.findElement(selector);
		} catch (NoSuchElementException e) {
			
			return false;
		}
		return true;
	}
	
	
	public static WebElement waitToBeClickable(WebDriver driver, By selector, int waitInterval) {
		WebElement element = (new WebDriverWait(driver, waitInterval)).until(ExpectedConditions.elementToBeClickable(selector));
		return element;
	}
	
	public static WebElement waitForElementPresence(WebDriver driver, By selector, int waitInterval) {
		WebElement element = (new WebDriverWait(driver, waitInterval)).until(ExpectedConditions.presenceOfElementLocated(selector));
		return element;
	}
	
	public static void waitForTitle(WebDriver driver, String title, int waitInterval){
		 (new WebDriverWait(driver, waitInterval)).until(ExpectedConditions.titleIs(title));
	}
	public static WebElement waitForDropDownElementsToBePresent(WebDriver driver,By selector,int waitinterval,String value,String value1) {
		//use explicit wait to make sure the element is there
		WebElement myDynamicElement = (new WebDriverWait(driver, waitinterval))
		.until(ExpectedConditions.presenceOfElementLocated(selector));
		Select dropdown = new Select(myDynamicElement);
		dropdown.selectByValue(value);
		dropdown.selectByValue(value1);
		return myDynamicElement;
		}
	public static boolean isAlertPresent(WebDriver driver) {
	    try {
	        driver.switchTo().alert();
	        return true;
	    } // try
	    catch (Exception e) {
	        return false;
	    } // catch
	}
	}



