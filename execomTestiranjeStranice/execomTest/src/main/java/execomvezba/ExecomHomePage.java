package execomvezba;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ExecomHomePage {
private WebDriver driver;

public ExecomHomePage(WebDriver driver) {
	super();
	this.driver = driver;
}
public WebElement getHeaderBtn() {
	return Utils.waitForElementPresence(driver, By.id("header-button"), 10);
}
public WebElement getCareers() {
	return Utils.waitForElementPresence(driver, By.id("careers-link"), 10);
}
//We like to challenge the status quo!
public WebElement getWeliketochallengethestatusquoText() {
	return Utils.waitForElementPresence(driver,By.cssSelector(".title"),10);
}
//.title.home-title
public WebElement getTitleText() {
	return Utils.waitToBeClickable(driver,By.cssSelector(".title.home-title"),10);
}
public boolean isTitleTextPresent() {
	return Utils.waitForElementPresence(driver,By.cssSelector(".title.home-title"),10).isDisplayed();
}
public boolean isSoftwareTesterTextPresent() {
	return Utils.waitForElementPresence(driver,By.cssSelector(".seed.black"),10).isDisplayed();
}
public String getSoftwareTesterTextToBeClicked() {
	return Utils.waitForElementPresence(driver,By.xpath("//a[text()=' Software Tester / Novi Sad, Subotica ']"),10).getText();
}

public WebElement  getSoftwareTesterText() {
	return Utils.waitToBeClickable(driver,By.cssSelector("#missing-seeds > div > div > a:nth-child(7)"),10);
}

}
